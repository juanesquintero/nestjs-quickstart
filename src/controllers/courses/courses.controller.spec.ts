import { Test, TestingModule } from '@nestjs/testing';
import { CoursesController } from './courses.controller';
import { Course } from './courses.dto';

describe('CoursesController', () => {
  let controller: CoursesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CoursesController],
    }).compile();

    controller = module.get<CoursesController>(CoursesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(typeof controller.getCourses()).toBe('Course[]');
    });
  });
});
