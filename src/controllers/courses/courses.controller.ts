import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  Req,
  Res,
} from '@nestjs/common';
import { Course } from './courses.dto';
import { CoursesService } from './courses.service';

@Controller('courses')
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @Get()
  getCourses(): Course[] {
    return this.coursesService.getCourses();
  }

  @Get('manual')
  manualHandleRequest(@Req() req, @Res() res): Course[] {
    return res.send(this.coursesService.getCourses());
  }

  @Get(':id')
  getCourse(@Param('id') id: string): Course {
    return this.coursesService.getCourse(id);
  }

  @Post()
  createCourse(@Body() body: Course): boolean {
    return this.coursesService.createCourse(body);
  }

  @Delete(':id')
  deleteCourse(@Param('id') id: string): boolean {
    return this.coursesService.deleteCourse(id);
  }

  @Put(':id')
  updateCourse(@Param('id') id: string, @Body() body: Course): boolean {
    return this.coursesService.updateCourse(id, body);
  }
}
