export type Course = {
  id: string;
  title: string;
  description: string;
  hours: number;
  price: number;
};
