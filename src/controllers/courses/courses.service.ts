import { Injectable } from '@nestjs/common';
import _ from 'lodash';
import crypto from 'crypto';
import coursesList from '../../db/courses.json';
import { Course } from './courses.dto';

@Injectable()
export class CoursesService {
  getCourses(): Course[] {
    return coursesList;
  }

  getCourse(id: string): Course {
    return _.filter(coursesList, { id: id });
  }

  createCourse(body: Course): boolean {
    body.id = crypto.randomBytes(16).toString('hex');
    coursesList.push(body);
    return true;
  }

  deleteCourse(id: string): boolean {
    const result = _.remove(coursesList, { id: id });
    return !!result;
  }

  updateCourse(id: string, body: Course): boolean {
    const index = _.findIndex(coursesList, { id: id });
    if (index) {
      coursesList.splice(index, 1, { id, ...body });
      return true;
    }
    return false;
  }
}
