import { Module } from '@nestjs/common';
import { HelloService } from './controllers/hello-world/hello.service';
import { HelloController } from './controllers/hello-world/hello.controller';
import { CoursesController } from './controllers/courses/courses.controller';
import { CoursesService } from './controllers/courses/courses.service';

@Module({
  imports: [],
  controllers: [HelloController, CoursesController],
  providers: [HelloService, CoursesService],
})
export class AppModule {}
